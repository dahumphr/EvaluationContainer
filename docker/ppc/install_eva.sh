module load ibm-wml-ce
export EVAHOME=$PWD/evaluationhome
export PYTHONPATH=/ATLASMLHPO/module/:/gpfs/alpine/proj-shared/csc343/zhangr/public/lib/:$EVAHOME/packages:/sw/summit/ibm-wml-ce/anaconda-base/envs/ibm-wml-ce-1.7.0-3/lib/python3.6/site-packages/:$PYTHONPATH
mkdir -p $EVAHOME/packages
pip install --upgrade pip
if [[ -f "$EVAHOME/../requirements.txt" ]]; then
    echo "Install customised requirements.txt"
    pip install -t $EVAHOME/packages --no-cache-dir -r $EVAHOME/../requirements.txt
fi
