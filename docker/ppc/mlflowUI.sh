#!/bin/sh

mlflow server \
    --backend-store-uri `cat /.tmp_MLFLOW_TRACKING_URI` \
    --host $MLFLOW_SERVER_HOST \
    --port $MLFLOW_SERVER_PORT
